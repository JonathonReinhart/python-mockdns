#!/usr/bin/env python3
from __future__ import print_function
import requests
import mockdns
import threading

# Python 3
from http.server import HTTPServer, SimpleHTTPRequestHandler
import socketserver

class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(b'This my intercepted HTTP response')

class BackgroundServer:
    def __init__(self, server):
        self.server = server
        self.thread = threading.Thread(target=self.server.serve_forever)

    def start(self):
        self.thread.start()

    def stop(self):
        self.server.shutdown()
        self.thread.join()


def main():
    port = 8888

    mockdns.add_dns_entry('icanhazip.com', ipv4='127.0.0.1')
    mockdns.install()

    httpd = BackgroundServer(HTTPServer(('', port), MyHandler))
    httpd.start()
    print("httpd running...")

    url = 'http://icanhazip.com:{}'.format(port)

    print('Requesting {}'.format(url))
    r = requests.get(url)
    r.raise_for_status()
    print(r.text)

    print("Stopping httpd...")
    httpd.stop()


if __name__ == '__main__':
    main()
