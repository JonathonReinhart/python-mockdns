from __future__ import print_function
import socket

real_getaddrinfo = socket.getaddrinfo

def install():
    socket.getaddrinfo = mock_getaddrinfo

class DnsEntry(object):
    def __init__(self, ipv4=None, ipv6=None):
        self.ipv4 = ipv4
        self.ipv6 = ipv6

_hosts = { }

def add_dns_entry(host, **kw):
    _hosts[host] = DnsEntry(**kw)

def mock_getaddrinfo(host, port, family=0, socktype=0, proto=0, flags=0):
    """Mocked version of socket.getaddrinfo

    Returns a list of 5-tuples with the following structure:
    (family, socktype, proto, canonname, sockaddr)
    """

    entry = _hosts.get(host)
    if entry is None:
        return real_getaddrinfo(host, port, family, socktype, proto, flags)

    if flags:
        raise NotImplementedError("flags must be zero")
    if not isinstance(port, int):
        raise NotImplementedError("port must be int")

    if socktype == 0:
        socktype = socket.SOCK_STREAM
    if proto == 0:
        proto = socket.IPPROTO_TCP

    cannonname = ''

    result = []

    if family in (0, socket.AF_INET6) and entry.ipv6:
        flow_info = 0
        scope_id = 0
        sockaddr = (entry.ipv6, port, flow_info, scope_id)
        info = (socket.AF_INET6, socktype, proto, cannonname, sockaddr)
        result.append(info)

    if family in (0, socket.AF_INET) and entry.ipv4:
        sockaddr = (entry.ipv4, port)
        info = (socket.AF_INET, socktype, proto, cannonname, sockaddr)
        result.append(info)

    return result
